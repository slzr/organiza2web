// MODULO ANGULAR APLICACION
angular.module('org2', ['ngDraggable'])

//  INICIALIZACION
.run(['$rootScope', 'getJson', function($rootScope, getJson) {
  
  getJson.materias()
    .success(function(response){
      console.log(response);
      $rootScope.mats = response;
    })
    .error(function(error){
      console.log(error);
    });



  $rootScope.horario = {
    1:[],
    2:[],
    3:[],
    4:[],
    5:[],
    6:[]
  };


  // FUNCION AL SOLTAR 
  $rootScope.onDrop = function($data, $event){
    console.log($data);
    angular.forEach($data.s.dias, function(value, pos){
      // console.log(value);
      $rootScope.horario[value.dia].push({
        v: value,
        p: $data.s.profesor,
        n: $data.n
      });
      //  console.log($rootScope.horario[value.dia]);
    });
  };
}])
//  INICIALIZACION


// CONFIGURACIONESS
.config(function( ) {
})


// DIRECTIVA PARA COLLAPSIBLE
.directive('collapsible', function() {
  return {
    'restrict' : 'A',
    template: [
      '<li ng-repeat="child in childs track by $index" >',
      '  <div class="collapsible-header bg-primary color-text over-hidden" title="{{child.nombre}}">',
      '    <i class="mdi-av-my-library-books"></i>{{child.nombre}}',
      '  </div>',
      '  <div class="collapsible-body">',
      '    <ul class="collection secciones">',
      '      <li ng-drag="true" ng-drag-data="{n: child.nombre, s: seccion}" ng-repeat="seccion in child.secciones track by $index" class="collection-item">Seccion {{seccion.seccion}}</li>',
      '    </ul>',
      '   </div>',
      '</li>'
    ].join(''),
    scope: {
      childs: "="
    },
    link: function(scope, element, atributes){
      scope.$watch('childs', function(n, v){
        //console.log(scope.childs)
        $(element[0]).collapsible({
          accordion : false 
        });
      });
    }
  };
})
// DIRECTIVA PARA COLLAPSIBLE


//  SERVICIO
.factory('getJson', ['$http', function($http){
  return {
    materias: function(){
      return $http.get('materias.json');
    }
  };
}])
//  SERVICIO
;
