var gulp           = require('gulp'),               //  GULP
    del            = require('del'),                //  DELETE FILES/FOLDERS
    gulpif         = require('gulp-if'),            //  GULP IF
    stylus         = require('gulp-stylus'),        //  GULP STYLUS PREPROCESSOR
    quills         = require('quills'),             //  STYLUS PLUGIN
    sourcemaps     = require('gulp-sourcemaps'),    //  CSS SOURCEMAPS
    autoprefixer   = require('gulp-autoprefixer'),  //  POSTCSS AUTOPREFIXER
    jshint         = require('gulp-jshint'),        //  JS LINT
    stylish        = require('jshint-stylish'),     //  JS LINT REPORTER
    useref         = require('gulp-useref'),        //  PARSE BUILD BLOCKS
    miniHTML       = require('gulp-minify-html'),   //  HTML MINIFY
    miniCss        = require('gulp-minify-css'),    //  CSS MINIFY
    uglify         = require('gulp-uglify'),        //  JS MINIFY
    imagemin       = require('gulp-imagemin'),      //  IMAGE MINIFY
    pngquant       = require('imagemin-pngquant'),  //  PNG MINIFY PLUGIN
    mainBowerFiles = require('main-bower-files'),   //  MAIN BOWER FILES
    connect        = require('gulp-connect'),       //  SERVER
    open           = require('gulp-open'),          //  OPEN BROWSER
    replace        = require('gulp-replace'),       //  REPLACE
    size           = require('gulp-size');          //  SIZE UTIL


//  CLEAN DEV FOLDERS
gulp.task('clean',            del.bind( null, ['dist'] ));
gulp.task('clean:components', del.bind( null, ['app/components'] ));
gulp.task('clean:modules',    del.bind( null, ['node_modules'] ));
gulp.task('clean:all', ['clean', 'clean:components', 'clean:modules'], null);
//  CLEAN DEV FOLDERS
 

//  LINT JS
gulp.task('lint', function(){
  return gulp.src('app/scripts/**/*.js')
    .pipe(jshint())
    .pipe(jshint.reporter(stylish))
    .pipe( connect.reload() );
});
//  LINT JS


//  COMPILE STYL FILES
 gulp.task('stylus', function(){
  return gulp.src('app/styles/*.styl')
    .pipe( stylus({ 'use': quills() }) )
    .pipe( gulp.dest('app/styles'))
    .pipe( connect.reload() );
 });
//  COMPILE STYL FILES


//  POSTCSS
 gulp.task('styles', ['stylus'], function() {
  return gulp.src('app/styles/*.css')
    .pipe( sourcemaps.init() )
    .pipe( autoprefixer({ browsers: ['ie 8', 'ie 9', 'last 2 versions', '> 5%'] }))
    .pipe( sourcemaps.write() )
    .pipe( gulp.dest('app/styles') )
    .pipe( connect.reload() );
});
//  POSTCSS


//  MINIFY, CONCAT HTML/CSS/JS & BOWER DEPS
gulp.task('minicat', ['lint', 'styles'], function() {
  var assets = useref.assets();
  return gulp.src('app/*.html')
    .pipe( assets )
    .pipe( gulpif('*.js',  uglify() ))
    .pipe( gulpif('*.css', replace("../font", "../fonts") ))
    .pipe( gulpif('*.css', miniCss({ compatibility: '*' }) ))
    .pipe( assets.restore() )
    .pipe( useref() )
    .pipe( gulpif('*.html', miniHTML({ conditionals: true, loose: true }) ))
    .pipe( gulp.dest('dist') );
});
//  MINIFY, CONCAT HTML/CSS/JS & BOWER DEPS


//  MINIFY ALL IMAGENES
gulp.task('imagemin', function(){
  return gulp.src('app/images/**/*')
    // .pipe( imagemin({
    //   progressive: true,
    //   interlaced: true,
    //   svgoPlugins: [{
    //     removeViewBox: false,
    //     cleanupIDs: false
    //   }],
    //   use: [pngquant()]
    // }))
    .pipe(gulp.dest('dist/images'));
});
//  MINIFY ALL IMAGENES


 // COPY ALL FONTS
gulp.task('fonts', function() {
  return gulp.src( mainBowerFiles({
    filter: '**/*.{eot,svg,ttf,woff,woff2}'
  }).concat('app/fonts/**/*'))
    .pipe(gulp.dest('dist/fonts/**/*'));
});
//  COPY ALL FONTS


//  COPY ALL EXTRAS FILES
gulp.task('extras', function() {
  return gulp.src([
    'app/*.*',
    '!app/*.html'
  ], { dot: true })
  .pipe(gulp.dest('dist'));
});
//  COPY ALL EXTRAS FILES


//  OPEN BROWSER
gulp.task('open-browser', function(){
  gulp.src(__filename)
  .pipe( open({
    // app: 'chrome',
    uri: 'http://localhost:1234'
  }));
});
//  OPEN BROWSER


//  SERVER WITH LIVERELOAD
gulp.task('serve', ['styles', 'open-browser'], function(){
  connect.server({
    root: ['app'],
    port: 1234,
    livereload: true,
  });

  gulp.watch('app/styles/**/*.styl', ['styles']);
  gulp.watch('app/scripts/**/*.js',  ['lint']);

  var files_to_watch = [
    'app/*.html',
    'app/scripts/**/*.js',
    'app/images/**/*',
    'app/fonts/**/*'
  ];

  gulp.watch(files_to_watch, function(){
    gulp.src(files_to_watch)
    .pipe( connect.reload() );
  });
});

gulp.task('serve:dist', ['open-browser'], function(){
    connect.server({
    root: ['dist'],
    port: 1234,
    livereload: true,
  });
});
//  SERVER WITH LIVERELOAD


//  BUILD APP 
gulp.task('build', ['minicat', 'imagemin', 'fonts', 'extras'], function() {
  return gulp.src('dist/**/*')
    .pipe(size({
      title: 'build',
      gzip: true
    })
  );
});
//  BUILD APP 


//  DEFAULT
gulp.task('default', function() {
  gulp.start('serve');
});
//  DEFAULT
