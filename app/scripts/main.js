
$(document).ready(function(){
    
    //  MATERIALIZE INICIALIZACIONES
    $('select').material_select();
    $('.parallax').parallax();
    //$(".button-collapse").sideNav();
    $('.collapsible').collapsible({
        accordion : true 
    });
    //  MATERIALIZE INICIALIZACIONES

    //    PARALLAX
    $(window).stellar({
        horizontalScrolling: false,
        verticalScrolling:true,
        responsive: true,
    });

});
